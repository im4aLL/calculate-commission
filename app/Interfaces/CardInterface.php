<?php

namespace App\Interfaces;

interface CardInterface
{
    /**
     * @param $bin
     * @return mixed
     */
    public function fetchByBin($bin);
}
