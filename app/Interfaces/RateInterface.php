<?php

namespace App\Interfaces;

interface RateInterface
{
    /**
     * Get latest rate by currency
     * @param $currency
     */
    public function getByCurrency($currency);
}
