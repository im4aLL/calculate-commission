<?php

namespace App\Interfaces;

interface FileReaderInterface
{
    /**
     * Set file path
     *
     * @param $filepath
     * @return $this
     */
    public function setFile($filepath);

    /**
     * Get file
     *
     * @return null
     */
    public function getFile();

    /**
     * Get array of data from file
     *
     * @return array
     */
    public function getData();
}
