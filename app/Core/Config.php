<?php
namespace App\Core;

class Config
{
    /**
     * @var array
     */
    private static $configArray;

    /**
     * @var null
     */
    private static $instance = null;

    private function __construct()
    {
        self::$configArray = include __DIR__.'/../../config.php';
    }

    /**
     * Get instance
     *
     * @return Config|null
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new Config();
        }

        return self::$instance;
    }

    /**
     * Get config value by name
     *
     * @param $name
     * @return mixed|null
     */
    public static function get($name)
    {
        $config = self::getAll();

        return self::getArrayValueByKey($name, $config);
    }

    /**
     * Get all config value
     *
     * @return array|mixed|null
     */
    public static function getAll()
    {
        $instance = self::getInstance();

        return isset($instance::$configArray) ? $instance::$configArray : null;
    }

    /**
     * Get array value by key
     *
     * @param $name
     * @param $array
     * @return mixed|null
     */
    private static function getArrayValueByKey($name, $array)
    {
        $result = null;

        foreach ($array as $key => $value) {
            if ($key === $name) {
                $result = $value;
                break;
            }
        }

        return $result;
    }
}
