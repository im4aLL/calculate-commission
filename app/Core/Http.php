<?php


namespace App\Core;


use GuzzleHttp\Client;

class Http
{
    /**
     * To call HTTP methods
     *
     * @var Client
     */
    private $httpClient;

    public function __construct()
    {
        $this->httpClient = new Client(['http_errors' => false]);
    }

    /**
     * Get http client
     *
     * @return Client
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * Convert guzzle http response body to json
     *
     * @param $response
     * @return bool|mixed
     */
    public function getResponseJson($response)
    {
        if ($response->getStatusCode() != 200) {
            return false;
        }

        $jsonString = $response->getBody()->getContents();
        return json_decode($jsonString);
    }
}
