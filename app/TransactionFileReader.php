<?php
namespace App;

use App\Interfaces\FileReaderInterface;

class TransactionFileReader implements FileReaderInterface
{
    /**
     * File path
     *
     * eg full path of index.txt
     */
    private $file;

    /**
     * Total data from file will store here
     *
     * @var array
     */
    private $data;

    /**
     * TransactionFileReader constructor.
     */
    public function __construct()
    {
        $this->file = null;
        $this->data = [];
    }

    /**
     * Set file path
     *
     * @param $filepath
     * @return $this
     */
    public function setFile($filepath)
    {
        $this->file = $filepath;

        return $this;
    }

    /**
     * Get file
     *
     * @return null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get array of data from file
     *
     * @return array
     */
    public function getData()
    {
        $lines = $this->getLines();

        if (count($lines) === 0) {
            return $this->data;
        }

        foreach ($lines as $line) {
            $this->data[] = json_decode($line);
        }

        return $this->data;
    }

    /**
     * Get lines of a txt file
     *
     * @return array
     */
    protected function getLines()
    {
        $lineArray = [];

        if ($this->file && file_exists($this->file)) {
            $fh = fopen($this->file, 'r');
            while ($line = fgets($fh)) {
                $lineArray[] = $line;
            }
            fclose($fh);
        }

        return $lineArray;
    }
}
