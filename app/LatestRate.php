<?php
namespace App;

use App\Core\Config;
use App\Core\Http;
use App\Interfaces\RateInterface;

class LatestRate implements RateInterface
{
    /**
     * Cached latest rate to avoid unnecessary multiple HTTP call
     *
     * @var null
     */
    public $cache;

    /**
     * Latest rate api URL
     *
     * @var string
     */
    private $url;

    /**
     * Http
     *
     * @var Http
     */
    private $http;

    public function __construct(Http $http)
    {
        $this->http = $http;
        $this->cache = null;
        $this->url = Config::get('rateHttpUrl');
    }

    /**
     * Get latest rate by currency
     *
     * @param $currency
     * @return int|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getByCurrency($currency)
    {
        if (!$this->cache) {
            $this->cache = $this->getLatestRate();
        }

        $rateData = $this->cache;

        return $this->getExchangeRateByCurrency($currency, $rateData->rates);
    }

    /**
     * Get cache data
     *
     * @return object | null
     */
    protected function getCache()
    {
        return $this->cache;
    }

    /**
     * Get latest rate
     *
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getLatestRate()
    {
        $response = $this->http->getHttpClient()->request('GET', $this->getUrl());

        return $this->http->getResponseJson($response);
    }

    /**
     * Get URL
     *
     * @return mixed|string|null
     */
    protected function getUrl()
    {
        return $this->url;
    }

    /**
     * Get exchange rate by currency
     *
     * @param $currency
     * @param $rates
     * @return int|mixed
     */
    protected function getExchangeRateByCurrency($currency, $rates)
    {
        $result = 0;

        foreach ($rates as $cur => $rate) {
            if ($cur === $currency) {
                $result = $rate;
                break;
            }
        }

        return $result;
    }
}
