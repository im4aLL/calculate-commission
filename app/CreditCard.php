<?php


namespace App;

use App\Core\Config;
use App\Core\Http;
use App\Interfaces\CardInterface;

class CreditCard implements CardInterface
{
    /**
     * Cached bin data to avoid unnecessary multiple HTTP call
     *
     * @var array
     */
    protected $cache;

    /**
     * Http
     *
     * @var Http
     */
    protected $http;

    /**
     * Bin data http base url
     *
     * @var string
     */
    protected $baseUrl;

    public function __construct(Http $http)
    {
        $this->cache = [];
        $this->http = $http;
        $this->baseUrl = Config::get('binHttpBaseUrl');
    }

    /**
     * Get card information by bin
     *
     * @param $bin
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function fetchByBin($bin)
    {
        $cachedBinData = $this->getCacheBykey($bin);

        if ($cachedBinData) {
            return $cachedBinData;
        } else {
            $data = $this->getBinData($bin);
            $this->addCache($bin, $data);

            return $data;
        }
    }

    /**
     * Fetch bin data by bin
     *
     * @param $bin
     * @return bool|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getBinData($bin)
    {
        $url = $this->getBinUrl($bin);
        $response = $this->http->getHttpClient()->request('GET', $url);

        return $this->http->getResponseJson($response);
    }

    /**
     * Get bin URL
     *
     * @param $bin
     * @return string
     */
    protected function getBinUrl($bin)
    {
        return $this->getBaseUrl().$bin;
    }

    /**
     * Get all cache data
     *
     * @return array
     */
    protected function getCache()
    {
        return $this->cache;
    }

    /**
     * Get cache data by key
     *
     * @param $bin
     * @return mixed|null
     */
    protected function getCacheBykey($bin)
    {
        $cacheData = $this->getCache();

        $result = null;

        foreach ($cacheData as $binNumber => $data) {
            if ($binNumber === $bin) {
                $result = $data;
                break;
            }
        }

        return $result;
    }

    /**
     * Add cache
     *
     * @param $bin
     * @param $data
     * @return CreditCard
     */
    protected function addCache($bin, $data)
    {
        $this->cache[$bin] = $data;

        return $this;
    }

    /**
     * Get base URL
     *
     * @return mixed|string|null
     */
    protected function getBaseUrl()
    {
        return $this->baseUrl;
    }
}
