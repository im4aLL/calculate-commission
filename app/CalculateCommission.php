<?php
namespace App;

use App\Core\Config;

class CalculateCommission
{
    /**
     * Total transactions found from given file
     *
     * @var array
     */
    private $transactions;

    /**
     * Commission value array
     *
     * @var array
     */
    private $result;

    /**
     * Base currency
     *
     * @var string
     */
    private $baseCurrency;

    /**
     * Base inside eu commission rate
     *
     * @var float
     */
    private $baseInsideCommissionRate;

    /**
     * Base outside eu commission rate
     *
     * @var float
     */
    private $baseOutsideCommissionRate;

    /**
     * Base region country codes
     *
     * @var array
     */
    private $baseRegionCountryCodes;

    /**
     * Transaction file reader
     *
     * @var TransactionFileReader
     */
    private $transactionFileReader;

    /**
     * Credit card class
     *
     * @var CreditCard
     */
    private $creditCard;

    /**
     * Lastest rate class
     *
     * @var LatestRate
     */
    private $latestRate;

    public function __construct(TransactionFileReader $transactionFileReader, CreditCard $creditCard, LatestRate $latestRate)
    {
        $this->transactions = [];
        $this->result = [];
        $this->transactionFileReader = $transactionFileReader;
        $this->creditCard = $creditCard;
        $this->latestRate = $latestRate;

        $config = Config::getAll();
        $this->setConfig((object) $config);
    }

    /**
     * Set config
     *
     * @param object $config
     * @return $this
     */
    public function setConfig($config)
    {
        $this->baseCurrency = $config->baseCurrency;
        $this->baseInsideCommissionRate = $config->baseInsideCommissionRate;
        $this->baseOutsideCommissionRate = $config->baseOutsideCommissionRate;
        $this->baseRegionCountryCodes = $config->baseRegionCountryCodes;

        return $this;
    }

    /**
     * Parse file and store commission value inside result
     *
     * @param $filepath
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function parse($filepath)
    {
        $this->transactions = $this->transactionFileReader->setFile($filepath)->getData();

        foreach ($this->transactions as $transaction) {
            $commisionAmount = $this->getCommissionAmountByTransaction($transaction);
            $this->result[] = $commisionAmount ? $this->roundAmount($commisionAmount) : 'Invalid bin or currency from input file';
        }

        return $this;
    }

    /**
     * Round amount with precision
     * Amount "0.44341965235899" will produce "0.45" when precision is 2
     *
     * @param $amount
     * @param int $precision
     * @return float|int
     */
    protected function roundAmount($amount, $precision = 2)
    {
        $fig = (int) str_pad('1', ($precision + 1), '0');
        return ceil($amount * $fig) / $fig;
    }

    /**
     * Get transactions
     *
     * @return array
     */
    public function getTransactions()
    {
        return $this->transactions;
    }

    /**
     * Get commission amount for each transaction
     *
     * @param $transaction
     * @return float|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    protected function getCommissionAmountByTransaction($transaction)
    {
        $result = null;

        $binData = null;
        try {
            $binData = $this->creditCard->fetchByBin($transaction->bin);
        } catch (Exception $e) {}

        if (!$binData) {
            return $result;
        }

        $isInsideBaseRegion = $this->isInsideBaseRegion($binData->country->alpha2);

        $rate = 0;
        try {
            $rate = $this->latestRate->getByCurrency($transaction->currency);
        } catch (Exception $e) {}

        $amount = 0;

        if ($transaction->currency === $this->getBaseCurrency() || $rate === 0) {
            $amount = $transaction->amount;
        } else if ($rate > 0) {
            $amount = $transaction->amount / $rate;
        }

        $result = $isInsideBaseRegion ? $amount * $this->getBaseInsideCommissionRate() : $amount * $this->getBaseOutsideCommissionRate();

        return $result;
    }

    /**
     * Get result array
     *
     * @return array
     */
    public function getResult() {
        return $this->result;
    }

    /**
     * If inside EU
     *
     * @param $countryCode
     * @return bool
     */
    protected function isInsideBaseRegion($countryCode) {
        if (!is_array($this->baseRegionCountryCodes)) {
            return false;
        }

        return in_array($countryCode, $this->getBaseRegionCountryCodes());
    }

    /**
     * Get base inside eu commission rate
     *
     * @return float
     */
    public function getBaseInsideCommissionRate()
    {
        return $this->baseInsideCommissionRate;
    }

    /**
     * Get base outside eu commission rate
     *
     * @return float
     */
    public function getBaseOutsideCommissionRate()
    {
        return $this->baseOutsideCommissionRate;
    }

    /**
     * Get base currency
     *
     * @return string
     */
    public function getBaseCurrency()
    {
        return $this->baseCurrency;
    }

    /**
     * Get base region counter code array
     *
     * @return array
     */
    public function getBaseRegionCountryCodes()
    {
        return $this->baseRegionCountryCodes;
    }
}
