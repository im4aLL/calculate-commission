<?php
use App\CalculateCommission;
use App\Core\Config;
use App\Core\Http;
use Dice\Dice;

require __DIR__.'/vendor/autoload.php';

$container = new Dice();
$container = $container->addRules([
    Http::class => ['shared' => true],
    Config::class => ['shared' => true],
]);
$calculateComissionInstance = $container->create(CalculateCommission::class);

if (isset($argv[1])) {
    $commissions = $calculateComissionInstance->parse($argv[1])->getResult();

    foreach ($commissions as $commission) {
        echo $commission;
        echo PHP_EOL;
    }
} else {
    echo 'You have missed to put file name';
}
