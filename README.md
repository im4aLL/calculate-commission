#### Run app
```
php app.php input.txt
```

#### To test app
```
./vendor/bin/phpunit --testdox tests
```

#### Expected result
```
1
0.45
1.65
2.3
44.48
```

#### Expected test result
```
PHPUnit 9.2.3 by Sebastian Bergmann and contributors.

Calculate Commission
 ✔ Should able to set config
 ✔ Should get commission amount by transaction
 ✔ Should parse file and store result
 ✔ Should round amount by precision
 ✔ Should return bool for is inside base region

Credit Card
 ✔ Should have base url after init
 ✔ Should have empty cache after init
 ✔ Should store cache
 ✔ Should fetch bin data by key

Latest Rate
 ✔ Should have url after init
 ✔ Should have empty cache after init
 ✔ Should get latest rate
 ✔ Should get exchange rate by currency
 ✔ Should get by currency
 ✔ Should store cache after get by currency

Transaction File Reader
 ✔ Should able to set a file
 ✔ Should convert lines to array
 ✔ Should have same line count

Time: 00:00.020, Memory: 6.00 MB

OK (18 tests, 27 assertions)
```
