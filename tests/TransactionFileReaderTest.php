<?php

use App\Interfaces\FileReaderInterface;
use App\TransactionFileReader;
use PHPUnit\Framework\TestCase;

class TransactionFileReaderForTest extends TransactionFileReader implements FileReaderInterface
{
    public function getLines()
    {
        return parent::getLines();
    }
}

class TransactionFileReaderTest extends TestCase
{
    public function testShouldAbleToSetAFile()
    {
        $file = 'a.txt';
        $transactionFileReader = new TransactionFileReader();
        $transactionFileReader->setFile($file);

        $this->assertEquals($transactionFileReader->getFile(), $file);
    }

    public function testShouldConvertLinesToArray()
    {
        $file = __DIR__.'./testinput.txt';
        $transactionFileReader = new TransactionFileReader();
        $data = $transactionFileReader->setFile($file)->getData();

        $this->assertTrue(is_array($data));
    }

    public function testShouldHaveSameLineCount()
    {
        $file = __DIR__.'./testinput.txt';
        $transactionFileReader = new TransactionFileReaderForTest();
        $data = $transactionFileReader->setFile($file)->getData();

        $this->assertEquals(count($data), count($transactionFileReader->getLines()));
    }
}
