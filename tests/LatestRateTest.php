<?php

use App\Core\Config;
use App\Core\Http;
use App\LatestRate;
use PHPUnit\Framework\TestCase;

class LatestRateForTest extends LatestRate
{
    public function getCache()
    {
        return parent::getCache();
    }

    public function getUrl()
    {
        return parent::getUrl();
    }

    public function getExchangeRateByCurrency($currency, $rates)
    {
        return parent::getExchangeRateByCurrency($currency, $rates);
    }

    public function getLatestRate()
    {
        return parent::getLatestRate();
    }
}

class LatestRateTest extends TestCase
{
    private $latestRate;

    public function setUp(): void
    {
        $http = new Http();

        $latestRateMockData = json_decode(file_get_contents(__DIR__.'./testrates.json'));
        $latestRate = $this->getMockBuilder(LatestRateForTest::class)
            ->setConstructorArgs([$http])
            ->onlyMethods(['getLatestRate'])
            ->getMock();
        $latestRate->method('getLatestRate')
            ->willReturn($latestRateMockData);

        $this->latestRate = $latestRate;
    }

    public function testShouldHaveUrlAfterInit()
    {
        $url = Config::get('rateHttpUrl');

        $this->assertEquals($url, $this->latestRate->getUrl());
    }

    public function testShouldHaveEmptyCacheAfterInit()
    {
        $this->assertNull($this->latestRate->getCache());
    }

    public function testShouldGetLatestRate()
    {
        $rateData = $this->latestRate->getLatestRate();

        $this->assertIsObject($rateData);
    }

    public function testShouldGetExchangeRateByCurrency()
    {
        $rateData = $this->latestRate->getLatestRate();
        $exchangeRate = $this->latestRate->getExchangeRateByCurrency('CAD', $rateData);
        $this->assertIsNumeric($exchangeRate);

        $exchangeRate = $this->latestRate->getExchangeRateByCurrency('AAA', $rateData);
        $this->assertEquals(0, $exchangeRate);
    }

    public function testShouldGetByCurrency()
    {
        $exchangeRate = $this->latestRate->getByCurrency('CAD');
        $this->assertIsNumeric($exchangeRate);

        $exchangeRate = $this->latestRate->getByCurrency('BBB');
        $this->assertEquals(0, $exchangeRate);
    }

    public function testShouldStoreCacheAfterGetByCurrency()
    {
        $this->latestRate->getByCurrency('CAD');
        $this->assertIsObject($this->latestRate->getCache());
    }
}
