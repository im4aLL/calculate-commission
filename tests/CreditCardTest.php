<?php

use App\Core\Config;
use App\Core\Http;
use App\CreditCard;
use PHPUnit\Framework\TestCase;

class CreditCardForTest extends CreditCard
{
    public function getBaseUrl()
    {
        return parent::getBaseUrl();
    }

    public function getCache()
    {
        return parent::getCache();
    }

    public function addCache($bin, $data)
    {
        return parent::addCache($bin, $data);
    }

    public function getCacheBykey($bin)
    {
        return parent::getCacheBykey($bin);
    }
}

class CreditCardTest extends TestCase
{
    private $creditCard;

    public function setUp(): void
    {
        $http = new Http();
        $binData = json_decode(file_get_contents(__DIR__.'./testbin.json'));
        $creditCard = $this->getMockBuilder(CreditCardForTest::class)
            ->setConstructorArgs([$http])
            ->onlyMethods(['getBinData'])
            ->getMock();
        $creditCard->method('getBinData')
            ->willReturn($binData);

        $this->creditCard = $creditCard;
    }

    public function testShouldHaveBaseUrlAfterInit()
    {
        $baseUrl = Config::get('binHttpBaseUrl');

        $this->assertEquals($baseUrl, $this->creditCard->getBaseUrl());
    }

    public function testShouldHaveEmptyCacheAfterInit()
    {
        $this->assertCount(0, $this->creditCard->getCache());
    }

    public function testShouldStoreCache()
    {
        $bin = 123;
        $binData = ['test' => 'data'];

        $this->creditCard->addCache($bin, $binData);
        $this->assertEquals($binData, $this->creditCard->getCacheBykey($bin));

        $bin2 = 456;
        $binData2 = ['test2' => 'data'];
        $this->creditCard->addCache($bin2, $binData2);
        $this->assertCount(2, $this->creditCard->getCache());
    }

    public function testShouldFetchBinDataByKey()
    {
        $bin = 123;
        $data = $this->creditCard->fetchByBin($bin);

        $this->assertIsObject($data);
        $this->assertSame($data, $this->creditCard->getCacheBykey($bin));
    }
}
