<?php
use App\CalculateCommission;
use App\Core\Config;
use App\Core\Http;
use App\CreditCard;
use App\LatestRate;
use App\TransactionFileReader;
use PHPUnit\Framework\TestCase;

class CalculateCommissionForTest extends CalculateCommission
{
    public function getCommissionAmountByTransaction($transaction)
    {
        return parent::getCommissionAmountByTransaction($transaction);
    }

    public function roundAmount($amount, $precision = 2)
    {
        return parent::roundAmount($amount, $precision);
    }

    public function isInsideBaseRegion($countryCode)
    {
        return parent::isInsideBaseRegion($countryCode);
    }
}

class CalculateCommissionTest extends TestCase
{
    private $calculateComission;
    private $filePath;
    private $http;

    private function getMockCreditCardInstance()
    {
        $binData = json_decode(file_get_contents(__DIR__.'./testbin.json'));
        $creditCard = $this->getMockBuilder(CreditCard::class)
            ->setConstructorArgs([$this->http])
            ->onlyMethods(['getBinData'])
            ->getMock();
        $creditCard->method('getBinData')
            ->willReturn($binData);

        return $creditCard;
    }

    private function getMockLatestRateInstance()
    {
        $latestRateMockData = json_decode(file_get_contents(__DIR__.'./testrates.json'));
        $latestRate = $this->getMockBuilder(LatestRate::class)
            ->setConstructorArgs([$this->http])
            ->onlyMethods(['getLatestRate'])
            ->getMock();
        $latestRate->method('getLatestRate')
            ->willReturn($latestRateMockData);

        return $latestRate;
    }

    public function setUp(): void
    {
        $this->filePath = __DIR__.'/testinput.txt';
        $this->http = new Http();

        $container = new Dice\Dice();
        $container = $container->addRules([
            CalculateCommissionForTest::class => [
                'substitutions' => [
                    CreditCard::class => $this->getMockCreditCardInstance(),
                    LatestRate::class => $this->getMockLatestRateInstance(),
                ]
            ]
        ]);

        $this->calculateComission = $container->create(CalculateCommissionForTest::class);
    }

    private function getDemoInputData()
    {
        $transactionFileReader = new TransactionFileReader();
        return $transactionFileReader->setFile($this->filePath)->getData();
    }

    public function testShouldAbleToSetConfig()
    {
        $config = Config::getAll();

        $this->assertEquals($config['baseCurrency'], $this->calculateComission->getBaseCurrency());
    }

    public function testShouldGetCommissionAmountByTransaction()
    {
        $data = $this->getDemoInputData();

        $commissionAmount = $this->calculateComission->getCommissionAmountByTransaction($data[0]);
        $this->assertEquals(1, $commissionAmount);

        $commissionAmount = $this->calculateComission->getCommissionAmountByTransaction($data[1]);
        $this->assertEquals(0.442164839052, $commissionAmount);
    }

    public function testShouldParseFileAndStoreResult()
    {
        $data = $this->getDemoInputData();

        $this->calculateComission->parse($this->filePath);
        $result = $this->calculateComission->getResult();

        $this->assertCount(count($data), $result);
    }

    public function testShouldRoundAmountByPrecision()
    {
        $this->assertEquals(0.45, $this->calculateComission->roundAmount(0.44444444));
        $this->assertEquals(0.45, $this->calculateComission->roundAmount(0.441111111));
        $this->assertEquals(0.44, $this->calculateComission->roundAmount(0.440000000));
        $this->assertEquals(100.00, $this->calculateComission->roundAmount(99.999999999));
    }

    public function testShouldReturnBoolForIsInsideBaseRegion()
    {
        $this->assertEquals(true, $this->calculateComission->isInsideBaseRegion('BG'));
        $this->assertEquals(false, $this->calculateComission->isInsideBaseRegion('BD'));
    }
}
