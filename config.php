<?php
return [
    'binHttpBaseUrl' => 'https://lookup.binlist.net/',
    'rateHttpUrl' => 'https://api.exchangeratesapi.io/latest',
    'baseCurrency' => 'EUR',
    'baseInsideCommissionRate' => 0.01,
    'baseOutsideCommissionRate' => 0.02,
    'baseRegionCountryCodes' => ['AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PO', 'PT', 'RO', 'SE', 'SI', 'SK'],
];
